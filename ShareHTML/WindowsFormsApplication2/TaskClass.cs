﻿using System;

namespace WindowsFormsApplication2
{
    [Serializable()]
    public abstract class TaskClass
    {
        public int Id { get; set; }
        public string NameOfTask { get; set; }
        public string Mail { get; set; }
        public string LinkHtml { get; set; }
        public string KeyWords { get; set; }
        public string Miasto { get; set; }
        public int Grad { get; set; }
        public int Val { get; set; }

        public abstract void DoTask();
    }
}
