namespace WindowsFormsApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class done : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ListOfTasks", "Miasto", c => c.String());
            AddColumn("dbo.ListOfTasks", "Grad", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ListOfTasks", "Grad");
            DropColumn("dbo.ListOfTasks", "Miasto");
        }
    }
}
