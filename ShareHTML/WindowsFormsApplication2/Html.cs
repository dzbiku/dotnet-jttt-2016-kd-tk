﻿using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace WindowsFormsApplication2
{
    internal class Html
    {
        public string Url { get; set; }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = WebUtility.HtmlDecode(wc.DownloadString(Url));
                return html;
            }
        }

        public string PrintPageNodes(string wordShare, string linkToJpg)
        {
            var documentOfHtml = new HtmlDocument();
            var downloadPageHtml = GetPageHtml();
            documentOfHtml.LoadHtml(downloadPageHtml);

            var nodesWithImg = documentOfHtml.DocumentNode.Descendants("img");

            foreach (var nodeWithImg in nodesWithImg)
            {
                string link = nodeWithImg.GetAttributeValue("src", "");
                string word = nodeWithImg.GetAttributeValue("alt", "");
                if (word.Contains(wordShare))
                    linkToJpg = link;
            }
            return linkToJpg;
            
        }
    }
}
