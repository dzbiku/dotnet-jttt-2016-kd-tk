﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public class Control
    {
        public static FileStream FStream { get; set; }
        public void ExitApplication()
        {
            if (MessageBox.Show(@"Do you want to exit?", @"All is done?",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                  == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        public void SerializeToFIle<T>(T obj, string dataFile)
        {
            using (FStream = File.Create(dataFile))
            {
                BinaryFormatter binSerialize =new BinaryFormatter();
                binSerialize.Serialize(FStream, obj);
            }
        }

        public T DeseriazlizeFromFile<T>(string dataFile)
        {
            T obj;
            using (FileStream fileStream = File.OpenRead(dataFile))
            {
                BinaryFormatter binSerializer =new BinaryFormatter();
                obj = (T) binSerializer.Deserialize(fileStream);
            }
                return obj;
        }
    }
}
