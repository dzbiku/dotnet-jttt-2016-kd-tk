﻿using System;

namespace WindowsFormsApplication2
{
    [Serializable()]
    public class Mail : TaskClass
    {
        public ListOfTask TaskList { get; set; }

        public Mail(string strName, string strMail, string strLinkHtml, string strKeyWords)
        {
            NameOfTask = strName;
            Mail = strMail;
            LinkHtml = strLinkHtml;
            KeyWords = strKeyWords;
            Val = 1;
        }

        public override void DoTask()
        {
            MailSender smtpPro = new MailSender();
            smtpPro.UserName = "test.netjava@gmail.com";
            smtpPro.SmptAdd = "smtp.gmail.com";
            smtpPro.Username = "test.netjava";
            smtpPro.Userpassword = "test@admin";

            var send = TaskList;
            smtpPro.Mail = send.Mail;
            smtpPro.LinkHtml = send.Link;
            smtpPro.KeyWords = send.Key;
            smtpPro.DoTask();

        }
    }
}