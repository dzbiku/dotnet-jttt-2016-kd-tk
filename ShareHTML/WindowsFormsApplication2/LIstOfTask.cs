﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    [Serializable()]
    public class ListOfTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Link { get; set; }
        public string Key { get; set; }

        public string Miasto { get; set; }
        public int Grad { get; set; }

        public int Val { get; set; }
        public List<TaskClass> ListTask;

        public ListOfTask()
        { ListTask = new List<TaskClass>(); }

        public void AddToList(string name, string mail, string link, string key, string miasto, int grad, int val)
        {
            if (val == 1)
            {
                var _mail = new Mail(name, mail, link, key);
                ListTask.Add(_mail);
            }
            if (val == 0)
            {
                var _temp = new WeatherShow(name, miasto, grad);
                ListTask.Add(_temp);
            }
        }

        public void SyncToListBox(ListBox oryginal)
        {
            oryginal.Items.Clear();
            foreach (var taskClass in ListTask)
            {
                if (taskClass.Mail != null)
                {
                    var mailowo = (Mail)taskClass;
                    oryginal.Items.Add($"NameTask={mailowo.NameOfTask}Mail={mailowo.Mail} Link={mailowo.LinkHtml} SlowoKlucz={mailowo.KeyWords}"); //string interpolation
                }
                else
                {
                    var temp = (WeatherShow) taskClass;
                    oryginal.Items.Add($"Name= {temp.NameOfTask} Miasto= {temp.Miasto} Stopnie= {temp.Grad} Bangla? {Val}");
                }
            }
        }

        public void ClearList()
        {
            ListTask.Clear();
        }
    }
}
