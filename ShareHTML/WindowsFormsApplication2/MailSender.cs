﻿using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public class MailSender : TaskClass
    {
        public string UserName { get; set; }
        public string SmptAdd { get; set; }
        public string Username { get; set; }
        public string Userpassword { get; set; }
        public Logger Logi = new Logger();

        public override void DoTask()
        {
            MailMessage mail;
            SmtpClient smtpServer;
            string linkWithAnserw;
            Proprety(out mail, out smtpServer, out linkWithAnserw);
            if (linkWithAnserw != null)
            {
                MailWithAttachment(mail, linkWithAnserw);
            }
            else
            {
                EmptyMail(mail);
            }
            EndOfSend(mail, smtpServer, linkWithAnserw);
        }

        private void EndOfSend(MailMessage mail, SmtpClient smtpServer, string linkWithAnserw)
        {
            smtpServer.Port = 587;
            smtpServer.Credentials = new NetworkCredential(Username, Userpassword); //"test.netjava" "test@admin"
            smtpServer.EnableSsl = true;
            smtpServer.Send(mail);
            MessageBox.Show(@"Link: " + linkWithAnserw); //"Spamujesz komus skrzynkę, bravo");
        }

        private void Proprety(out MailMessage mail, out SmtpClient smtpServer, out string linkWithAnserw)
        {
            mail = new MailMessage
            {
                From = new MailAddress(this.UserName),
                Subject = "Przeszukuje strone: " + LinkHtml
            };
            smtpServer = new SmtpClient(SmptAdd);
            mail.To.Add(Mail);
            var htmlShare = new Html();
            htmlShare.Url = LinkHtml;
            linkWithAnserw = htmlShare.PrintPageNodes(KeyWords, base.LinkHtml);
        }

        private void MailWithAttachment(MailMessage mail, string linkWithAnserw)
        {
            mail.Body = "Szukana fraza: " + KeyWords + " Link do obrazka: " + linkWithAnserw;


            var jpgLocalFileName = KeyWords;
            DownloadPicture(linkWithAnserw, jpgLocalFileName);

            var attachment = new Attachment(jpgLocalFileName, MediaTypeNames.Image.Jpeg);
            mail.Attachments.Add(attachment);
            Logi.Loggerhtml("Wyslano maila z obrazkiem i linkiem" + "\r\n Mail do " + Mail);
            Logi.Loggerhtml("Przeszukano strone: " + LinkHtml + "\r\n Szukano " + KeyWords);
        }

        public void DownloadPicture(string linkWithAnserw, string jpgLocalFileName)
        {
            var nocos = new WebClient();
            nocos.DownloadFile(linkWithAnserw, jpgLocalFileName);
        }

        private void EmptyMail(MailMessage mail)
        {
            mail.Body = "**************NIEZNALEZIONO****************";
            Logi.Loggerhtml("Wyslano maila o braku obrazka" + "\r\n Mail do " + Mail);
            Logi.Loggerhtml("Przeszukano strone: " + LinkHtml + "\r\n Szukano " + KeyWords);
        }

    }
}
