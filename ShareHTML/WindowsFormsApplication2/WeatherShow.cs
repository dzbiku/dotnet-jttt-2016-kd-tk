﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    [Serializable()]
    public class WeatherShow : TaskClass
    {
        public double _JSON { get; set; }
        public string Pic { get; set; }
        public WeatherShow(string name, string miasto, int grad)
        {
            NameOfTask = name;
            Miasto = miasto;
            Grad = grad;
            Val = 0;
        }
        public override void DoTask()
        {
           // MessageBox.Show(@"dziala");

            string json = "http://api.openweathermap.org/data/2.5/weather?q=" + Miasto + "&APPID=6f9ae208ecaee66e0cee0961a021fa18";
            var textFromFile = (new WebClient()).DownloadString(json);
            WeatherJson.RootObject weather = JsonConvert.DeserializeObject<WeatherJson.RootObject>(textFromFile);

            _JSON = weather.main.temp - 273;
            Pic = "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png";
        }

        public void ShowTemp()
        {
            if (Grad < _JSON)
            {
                MessageBox.Show($"We {Miasto} jest {_JSON} stopni \n Jest wiecej niz myslales");
            }
            else if (Grad > _JSON)
            {
                MessageBox.Show($"We {Miasto} jest {_JSON} stopni \n Jest mniej niz myslales");
            }
        }
    }
}
