﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    //usuwanie obrazka z dysku
    //zmiana na BindingList
    //dodawac obiekty do listy

    public partial class Form1 : Form
    {
        public string WordShareOnPage, Link, TraceToJpg;
        public Logger Logi = new Logger();
        public ListOfTask ListOf = new ListOfTask();
        public Mail DoTaskMail;
        public Control Co = new Control();
        public Form1()
        {
            InitializeComponent();
        }

        public Form1(string wordShareOnPage, string link, string traceToJpg, Logger logi, ListOfTask listOf, Mail doTaskMail, Control co)
        {
            WordShareOnPage = wordShareOnPage;
            Link = link;
            TraceToJpg = traceToJpg;
            Logi = logi;
            ListOf = listOf;
            DoTaskMail = doTaskMail;
            Co = co;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1 == null)
            {
                CheckTextBox();
            }

            if (!checkBox1.Checked && !checkWeather.Checked)
            {
                MessageBox.Show(@"You must select some options!");
            }
            DoEverything();
            Co.ExitApplication();
        }



        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddToList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked) return;
            tabPage2.Enabled = false;
            tabPage2.Visible = false;
            tabPage1.Enabled = true;
            tabPage1.Visible = true;
        }

        private void checkWeather_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkWeather.Checked) return;
            tabPage1.Enabled = false;
            tabPage1.Visible = false;
            tabPage2.Enabled = true;
            tabPage2.Visible = true;
        }

        private void DoEverything()
        {
            foreach (var taskClass in ListOf.ListTask)
            {
                if (taskClass.Val == 1)
                {
                    DoTaskMail = new Mail(textName.Text, textBox1.Text, textBox2.Text, textBox3.Text);
                    DoTaskMail.TaskList = ListOf;
                    DoTaskMail.DoTask();
                }
                else
                {
                    var temp = (WeatherShow)taskClass;
                    temp.DoTask();
                    temp.ShowTemp();
                }
            }
        }
        private void buttonSer_Click(object sender, EventArgs e)
        {
            Co.SerializeToFIle<ListOfTask>(ListOf, "LISTDATA.data");
            MessageBox.Show(@"Looks OK!");
        }
        private void buttonDeSer_Click(object sender, EventArgs e)
        {
            var newDe = Co.DeseriazlizeFromFile<ListOfTask>("LISTDATA.data");
            ListOf = newDe;
            newDe.SyncToListBox(listBox1);
            MessageBox.Show(@"Let's continue with old data... ");
        }

        private void ClearAll()
        {
            ListOf.ClearList();
            listBox1.Items.Clear();
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textName.Clear();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void miastoBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void AddToList()
        {
            if (ListOf == null) return;
            ListOf.Name = textName.Text;
            ListOf.Mail = textBox1.Text;
            ListOf.Link = textBox2.Text;
            ListOf.Key = textBox3.Text;
            ListOf.Miasto = miastoBox.Text;
            ListOf.Grad = Convert.ToInt32(gradBox.Text);
            if (checkBox1.Checked)
            {
                ListOf.Val = 1;
            }
            if (checkWeather.Checked)
            {
                ListOf.Val = 0;
            }

            using (var context = new JtttContext())
            {
                context.ListOfTasks.Add(ListOf);
                context.SaveChanges();
            }
            ListOf.AddToList(textName.Text, textBox1.Text, textBox2.Text, textBox3.Text, miastoBox.Text, int.Parse(gradBox.Text), ListOf.Val);
            ListOf.SyncToListBox(listBox1);
        }
        private void CheckTextBox()
        {
            if (textBox1.Text == string.Empty)
                button1.Text = @"Podaj email";
            else if (textBox2.Text == string.Empty)
                button1.Text = @"Podaj link";
            else if (textBox3.Text == string.Empty)
                button1.Text = @"Leo WHY?";
        }
    }

}
